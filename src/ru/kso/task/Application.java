package ru.kso.task;

public class Application {

    private static final String HELLO_MESSAGE = "Hello World";

    public static void main(String[] args) {
        System.out.println(HELLO_MESSAGE);
    }
}
